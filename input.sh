#!/bin/bash
file="/var/log/$1"
n=1
cd "/home/cloudera/apache-ignite-fabric-2.3.0-bin/bin"
echo "CREATE TABLE $1 (id LONG PRIMARY KEY, name VARCHAR)" |  ./sqlline.sh --color=true --verbose=true -u jdbc:ignite:thin://127.0.0.1/


while read line
do
# display $line or do somthing with $line

echo "INSERT INTO $1 (id,name) VALUES ($n,'$line') " |  ./sqlline.sh --color=true --verbose=true -u jdbc:ignite:thin://127.0.0.1/
((n++))

done < $file
#echo "$file"
