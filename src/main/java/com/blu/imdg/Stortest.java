package com.blu.imdg;

import java.io.Serializable;

import org.apache.ignite.cache.query.annotations.QuerySqlField;
/**
 * 
 * Stortest.java
 * 
 * Purpose: Describes data stored  in SQL table
 *
 * @author Natatya Galkina
 * @version 1.0 1/12/17
 */
public class Stortest implements Serializable{
	private static final long serialVersionUID = 1L;

	/** Will be indexed in ascending order.
	 * ID of log line
	 *  */
	@QuerySqlField(index = true)
  private long id;
  
  /** 
   * Content of log line
   * Will be visible in SQL, but not indexed. */
  @QuerySqlField
  private String name;


	
	

}
