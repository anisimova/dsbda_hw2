package com.blu.imdg;

import java.util.ArrayList;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * 
 * MySpark.java
 * 
 * Purpose: Main class.It gets data, provided by MyIgnite.java and compute syslog priority
 *
 * @author Natatya Galkina
 * @version 1.0 1/12/17
 */



public class MySpark {
	
	/**
	 * Main class.   It gets data, provided by MyIgnite.java and compute syslog priority                        
	 * <p>
	 *Creates MyIgnite instance. Get ArrayList of Strings from it. Than converts is to JavaRDD.
	 *Execute 8 filters to get only informative lines of log.
	 *Each filter looks for lines of its priority.
	 *Then all priority types are counted and send to System.out.print 
	 *
	 * @param  args[0] - String name of table to load from Ignite
	 * @return void.
	 */
	
	 
	public static void main(String[] args) {
		

			/**
		 	* Init spark conf
		 	*/
		 	SparkConf sparkConf = new SparkConf().setAppName("Line Count");
		 	sparkConf.setMaster("local");
		 
		 	/**
		 	 * Init spark context
		 	 */
		 	JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);
		 
		 	/**
		 	 * Init MyIgnite instance
		 	 */		
			MyIgnite ign = new MyIgnite();
			
			 /**
			  * Get list of Strings from Ignite
			  */
			ArrayList<String> list =  ign.readData(args[0]);
			 /**
			  * Convert lines to RDD
			  */
			JavaRDD<String> lines = sparkContext.parallelize(list);
			
			
			
			 /**
			  * Generate new RDDs. Each will contain only lines of single log priority
			  */
			
			JavaRDD<String> debug = lines.filter(MyFilter.getDebugFilter());
			JavaRDD<String> info = lines.filter(MyFilter.getInfoFilter());
			JavaRDD<String> notice = lines.filter(MyFilter.getNoticeFilter());
			JavaRDD<String> warn = lines.filter(MyFilter.getWarnFilter());
			JavaRDD<String> err = lines.filter(MyFilter.getErrFilter());
			JavaRDD<String> crit = lines.filter(MyFilter.getCritFilter());
			JavaRDD<String> alert = lines.filter(MyFilter.getAlertFilter());
			JavaRDD<String> panic = lines.filter(MyFilter.getPanicFilter());
			
			 /**
			  * Calculates each priority
			  */
			
			Long numDebug = debug.count();
			Long numInfo = info.count();
			Long numNotice = notice.count();
			Long numWarn = warn.count();
			Long numErr = err.count();
			Long numCrit = crit.count();
			Long numAlert = alert.count();
			Long numPanic = panic.count();
			
			
			 /**
			  * Close spark context
			  */
			sparkContext.close();
			
			 /**
			  * Print out result
			  */
			System.out.println("DEBUG:		"+ numDebug);
			System.out.println("INFO:		"+ numInfo);
			System.out.println("NOTICE:		"+ numNotice);
			System.out.println("WARN:		"+ numWarn);
			System.out.println("ERR:		"+ numErr);
			System.out.println("CRIT:		"+ numCrit);
			System.out.println("ALERT:		"+ numAlert);
			System.out.println("PANIC:		"+ numPanic);

			
	}
}
