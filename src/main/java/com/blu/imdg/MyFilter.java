package com.blu.imdg;
import org.apache.spark.api.java.function.Function;

/**
 * 
 * MyFiler.java
 * 
 * Purpose: Implements filter function for Spark process 
 *
 * @author Natatya Galkina
 * @version 1.0 1/12/17
 */
class MyFilter {

	/**
	 *   Filter only lines of DEBUG                         
	 */
	static Function<String, Boolean> debugFilter = new Function<String, Boolean>() {
		

		private static final long serialVersionUID = 1L;
		/**
		 *   Filter only lines of DEBUG                        
		 *
		 * @param  String str - String to filter
		 * @return Boolean - true if it is a string of DEBUG
		 */
		@Override
		public Boolean call(String str) throws Exception {
			// TODO Auto-generated method stub
			 return (str.contains("<debug>" ) || str.contains(" DEBUG " ) || str.contains(" DEBUG:" ));
		}
	};
	/**
	 *   Filter only lines of INFO                        
	 */
		
	static Function<String, Boolean> infoFilter = new Function<String, Boolean>() {
		
	
		private static final long serialVersionUID = 1L;
		/**
		 *   Filter only lines of INFO                        
		 *
		 * @param  String str - String to filter
		 * @return Boolean - true if it is a string of INFO
		 */
		@Override
		public Boolean call(String str) throws Exception {
			// TODO Auto-generated method stub
			 return (str.contains("<info>" ) || str.contains(" INFO " ) || str.contains(" INFO:" ));
		}
	};
	/**
	 *   Filter only lines of NOTICE                        
	 */
	static Function<String, Boolean> noticeFilter = new Function<String, Boolean>() {
		

		private static final long serialVersionUID = 1L;
		/**
		 *   Filter only lines of NOTICE                        
		 *
		 * @param  String str - String to filter
		 * @return Boolean - true if it is a string of NOTICE
		 */
		@Override
		public Boolean call(String str) throws Exception {
			// TODO Auto-generated method stub
			 return (str.contains("<notice>" ) || str.contains(" NOTICE " ) || str.contains(" NOTICE:" ));
		}
	};	
	/**
	 *   Filter only lines of WARN                        
	 */
	static Function<String, Boolean> warnFilter = new Function<String, Boolean>() {
		

		private static final long serialVersionUID = 1L;
		/**
		 *   Filter only lines of WARN                        
		 *
		 * @param  String str - String to filter
		 * @return Boolean - true if it is a string of WARN
		 */
		@Override
		public Boolean call(String str) throws Exception {
			// TODO Auto-generated method stub
			 return (str.contains("<warn>" ) || str.contains(" WARN " ) || str.contains(" WARN:" ) 
					 || str.contains("<warning>" ) || str.contains(" WARNING " ) || str.contains(" WARNING:" ));
		}
	};	
	
	/**
	 *   Filter only lines of ERR                        
	 */
	static Function<String, Boolean> errFilter = new Function<String, Boolean>() {
		

		private static final long serialVersionUID = 1L;
		/**
		 *   Filter only lines of ERR                        
		 *
		 * @param  String str - String to filter
		 * @return Boolean - true if it is a string of ERR
		 */
		@Override
		public Boolean call(String str) throws Exception {
			// TODO Auto-generated method stub
			 return (str.contains("<err>" ) || str.contains(" ERR " ) || str.contains(" ERR:" ) 
					 || str.contains("<error>" ) || str.contains(" ERROR " ) || str.contains(" ERROR:" ));
		}
	};

	/**
	 *   Filter only lines of CRIT                        
	 */
	static Function<String, Boolean> critFilter = new Function<String, Boolean>() {
		

		private static final long serialVersionUID = 1L;
		/**
		 *   Filter only lines of CRIT                        
		 *
		 * @param  String str - String to filter
		 * @return Boolean - true if it is a string of CRIT
		 */
		@Override
		public Boolean call(String str) throws Exception {
			// TODO Auto-generated method stub
			 return (str.contains("<crit>" ) || str.contains(" CRIT " ) || str.contains(" CRIT:" ));
		}
	};	
	/**
	 *   Filter only lines of ALERT                        
	 */
	static Function<String, Boolean> alertFilter = new Function<String, Boolean>() {
		

		private static final long serialVersionUID = 1L;
		/**
		 *   Filter only lines of DEBUG                        
		 *
		 * @param  String str - String to filter
		 * @return Boolean - true if it is a string of ALERT
		 */
		@Override
		public Boolean call(String str) throws Exception {
			// TODO Auto-generated method stub
			 return (str.contains("<alert>" ) || str.contains(" ALERT " ) || str.contains(" ALERT:" ));
		}
	};	
	
	/**
	 *   Filter only lines of PANIC                        
	 */
	static Function<String, Boolean> panicFilter = new Function<String, Boolean>() {
		

		private static final long serialVersionUID = 1L;
		/**
		 *   Filter only lines of PANIC                        
		 *
		 * @param  String str - String to filter
		 * @return Boolean - true if it is a string of PANIC
		 */
		@Override
		public Boolean call(String str) throws Exception {
			// TODO Auto-generated method stub
			 return (str.contains("<emerg>" ) || str.contains(" EMERG " ) || str.contains(" EMERG:" ) 
					 || str.contains("<panic>" ) || str.contains(" PANIC " ) || str.contains(" PANIC:" ));
		}
	};

	
	
	/**
	 *   Filer getter                        
	 *
	 * @param  No
	 * @return InfoFiler instance
	 */	
	public static Function<String, Boolean> getInfoFilter() {
		return infoFilter;
	}



	/**
	 *   Filer getter                        
	 *
	 * @param  No
	 * @return DebugFiler instance
	 */
	public static Function<String, Boolean> getDebugFilter() {
		return debugFilter;
	}



	/**
	 *   Filer getter                        
	 *
	 * @param  No
	 * @return NoticeFiler instance
	 */
	public static Function<String, Boolean> getNoticeFilter() {
		return noticeFilter;
	}



	/**
	 *   Filer getter                        
	 *
	 * @param  No
	 * @return WarnFiler instance
	 */
	public static Function<String, Boolean> getWarnFilter() {
		return warnFilter;
	}



	/**
	 *   Filer getter                        
	 *
	 * @param  No
	 * @return ErrFiler instance
	 */
	public static Function<String, Boolean> getErrFilter() {
		return errFilter;
	}



	/**
	 *   Filer getter                        
	 *
	 * @param  No
	 * @return CritFiler instance
	 */
	public static Function<String, Boolean> getCritFilter() {
		return critFilter;
	}



	/**
	 *   Filer getter                        
	 *
	 * @param  No
	 * @return AlertFiler instance
	 */
	public static Function<String, Boolean> getAlertFilter() {
		return alertFilter;
	}



	/**
	 *   Filer getter                        
	 *
	 * @param  No
	 * @return PanicFiler instance
	 */
	public static Function<String, Boolean> getPanicFilter() {
		return panicFilter;
	}
	
	
	
	
	
}
