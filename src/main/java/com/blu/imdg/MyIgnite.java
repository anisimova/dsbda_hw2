package com.blu.imdg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.ignite.*;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;

/**
 * 
 * MySpark.java
 * 
 * Purpose: Invokes Ignite client to get some data from Native Ignite persistence 
 *
 * @author Natatya Galkina
 * @version 1.0 1/12/17
 */
public class MyIgnite {
	
	/**
	 *   It gets data from Native Ignite Persistence                        
	 * <p>
	 * Invoke Ignite Client. Reads lines of Syslog from table with provided name. 
	 * To look at table properties go to Stortest.java class.
	 *    
	 *
	 * @param  String dbName - String name of table to load from Ignite
	 * @return ArrayList<String> - Lines of linux Syslog.
	 */
	
	ArrayList<String> readData(String dbName) {
      
		/**
	 	* Create a new instance of TCP Discovery SPI
	 	*/ 
        TcpDiscoverySpi spi = new TcpDiscoverySpi();
        
        /**
	 	* Create a new instance of tcp discovery multicast ip finder
	 	*/
        TcpDiscoveryMulticastIpFinder tcMp = new TcpDiscoveryMulticastIpFinder();
        tcMp.setAddresses(Arrays.asList("localhost")); 
        /**
	 	* Set the multi cast ip finder for spi
	 	*/ 
        spi.setIpFinder(tcMp);
        /**
	 	* create new ignite configuration
	 	*/ 
        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setClientMode(true);
        /**
	 	* set the discovery§ spi to ignite configuration
	 	*/ 
        cfg.setDiscoverySpi(spi);
        /**
	 	* Start ignite
	 	*/
        Ignite ignite = Ignition.start(cfg);
        ignite.active(true);
        /**
	 	* Get cache of asked table
	 	*/
        IgniteCache<Long, Stortest> cache = ignite.getOrCreateCache("SQL_PUBLIC_"+ dbName);
        /**
	 	* SQL select lines from table
	 	*/
        SqlFieldsQuery sql = new SqlFieldsQuery(      
        		"SELECT name FROM "+ dbName);

        /**
	 	* A list to store lines
	 	*/
        ArrayList <String> list = new ArrayList <String>(); 		 
        /**
	 	* Adding lines into list 
	 	*/ 
        try (QueryCursor<List<?>> cursor =  cache.query(sql)) {
            for (List<?> row : cursor)
            
            	list.add(row.toString());
        }
        
        /**
	 	* Close connection 
	 	*/        
        ignite.close();
        return list;
    }
}