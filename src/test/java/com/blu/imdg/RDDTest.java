package com.blu.imdg;

import static org.junit.Assert.*;

import java.io.Serializable;
import java.util.*;

import org.apache.spark.api.java.JavaRDD;
import org.junit.Test;
import com.holdenkarau.spark.testing.SharedJavaSparkContext;


public class RDDTest extends SharedJavaSparkContext implements Serializable {
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Test
	  public void verifyTest1() {
	    ArrayList<String> input = new ArrayList<String>();
	    input.add("17/12/01 00:39:29 INFO util.ShutdownHookManager: Shutdown hook called");
	    input.add("17/12/01 00:39:29  util.ShutdownHookManager: Shutdown hook called");
	    input.add("17/12/01 00:39:29INFO   util.ShutdownHookManager: Shutdown hook called");
	    JavaRDD<String> inputRDD = jsc().parallelize(input);
	    JavaRDD<String> result = inputRDD.filter(MyFilter.getInfoFilter());
	    assertEquals(1, result.count());
	  }

	@Test
	  public void verifyTest2() {
	    ArrayList<String> input = new ArrayList<String>();
	    input.add("17/12/01 00:39:29 INFO util.ShutdownHookManager: Shutdown hook called");
	    input.add("17/12/01 00:39:29  util.ShutdownHookManager: Shutdown hook called");
	    JavaRDD<String> inputRDD = jsc().parallelize(input);
	    JavaRDD<String> result = inputRDD.filter(MyFilter.getErrFilter());
	    assertEquals(0, result.count());
	  }
	@Test
	  public void verifyTest3() {
	    ArrayList<String> input = new ArrayList<String>();
	    input.add("");
	   // input.add("17/12/01 00:39:29  util.ShutdownHookManager: Shutdown hook called");
	    JavaRDD<String> inputRDD = jsc().parallelize(input);
	    JavaRDD<String> result = inputRDD.filter(MyFilter.getInfoFilter());
	    assertEquals(0, result.count());
	  }
	
	}